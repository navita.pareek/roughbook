const globalData = {};

chrome.runtime.onInstalled.addListener(function (object) {
  if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia({audio: true, video: false})
    .then((response) => console.log(response))
    .catch((error) => {
      chrome.tabs.create({
        url: "getAccess.html"
      })
    })
  }
});

function updateMicrophones() {
  console.log("called to update mic");
  if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia({audio: true, video: false})
    .then((response) => console.log(response))
    .catch((error) => console.log("error while accessing mic in background.js: ", error))
  }
}

chrome.runtime.onMessage.addListener(data => {
  if(data.type === 'notification') {
    globalData[data.options.eventTime.toString()] = data.options;
    chrome.alarms.create(data.options.eventTime.toString(), {
      when: data.options.eventTime,
    });
  } else if(data.type === 'micAccess') {
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({audio: true, video: false})
      .then((response) => console.log(response))
      .catch((error) => {
        chrome.tabs.create({
          url: "getAccess.html"
        })
      })
    }
  }
});

chrome.alarms.onAlarm.addListener((alarm) => {
  console.log("alarm.name: ", alarm.name);
  console.log("globalData[alarm.name]: ", globalData[alarm.name]);
  console.log("Last error1: ", chrome.runtime.lastError);
  chrome.notifications.create(alarm.name, globalData[alarm.name], (notificationId) => {console.log("notificationId: ", notificationId); console.log("Last error2: ", chrome.runtime.lastError);});
  console.log("Last error3: ", chrome.runtime.lastError);
});