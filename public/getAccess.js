$(document).ready(function() {
    function updateAllDevices(response) {
        let bg = chrome.extension.getBackgroundPage();
        bg.updateMicrophones();
    }

    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({audio: true, video: false})
        .then((response) => updateAllDevices(response))
        .catch((error) => console.log("error while accessing mic: ", error))
    }
});