/*global chrome*/
import React, { useContext, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import { AppContext } from './app-context/app-context-provider';
import Box from '@material-ui/core/Box';
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Tooltip from '@material-ui/core/Tooltip';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DeleteIcon from '@material-ui/icons/Delete';
import SettingsIcon from '@material-ui/icons/Settings';
import Drawer from '@material-ui/core/Drawer';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RefreshIcon from '@material-ui/icons/Refresh';
import Tour from 'reactour';
import Popper from '@material-ui/core/Popper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import slack from './slackIcon32.png';
import slackDisabled from './slackIconDisabled32.png';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import LinkIcon from '@material-ui/icons/Link';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import MicIcon from '@material-ui/icons/Mic';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import StopIcon from '@material-ui/icons/Stop';
import { ReactMic } from 'react-mic';
import ReactAudioPlayer from 'react-audio-player';
import {
  EmailShareButton,
  EmailIcon,
} from "react-share";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import PlayForWorkIcon from '@material-ui/icons/PlayForWork';
import NotListedLocationIcon from '@material-ui/icons/NotListedLocation';
import EditIcon from '@material-ui/icons/Edit';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles({
  Disabled: {
    pointerEvents: "none", 
    opacity: "0.5"
  },
  AppExtension: {
    width: '502px'
  },
  App: {
    width: '85%',
    ['@media (max-width: 1224px)']: {
      width: '100%'
    },
    margin: 'auto',
  },
  Footer: {
    backgroundColor: '#282c34',
    height: '45px',
    textAlign: 'center',
    padding: '10px',
    width: '100%',
    ['@media (max-width: 1224px)']: {
      width: '94.5%'
    },
  },
  ExtensionHeader: {
    backgroundColor: '#282c34',
    height: '52px',
    textAlign: 'start',
    padding: '4px',
    width: '100%',
    ['@media (max-width: 1224px)']: {
      width: '97.7%'
    },
  },
  SettingsWrapper: {
    padding: '10px',
    backgroundColor: '#282c34',
    height: '100%'
  },
  ExtensionLogo: {
    paddingLeft: '6px',
    paddingRight: '6px'
  },
  HistoryWrapper: {
    maxHeight: '300px',
    overflow: 'scroll',
    padding: '20px',
  },
  DeletedNotesWrapper: {
    width: '100%',
    height: '190px',
    bottom: '0',
    overflow: 'scroll',
    padding: '10px',
    wordWrap: 'break-word'
  },
  OldNotesWrapper: {
    height: '190px',
    top: '60px',
    bottom: '0',
    overflow: 'scroll',
    padding: '10px',
    wordWrap: 'break-word',
    marginBottom: '2px'
  },
  OldNotesWrapperExtension: {
    height: '250px',
    top: '60px',
    bottom: '0',
    overflow: 'scroll',
    padding: '10px',
    wordWrap: 'break-word',
    marginBottom: '2px'
  },
  NewNotes: {
    width: '100%',
    margin: '0px'
  },
  dialogInputText: {
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  title: {
    color: '#ffffff'
  },
  email: {
    color: "#b2ff59",
    marginLeft: '43px'
  },
  register: {
    color: "#b2ff59",
  },
  dateTime: {
    fontSize: 'x-small'
  },
  notes: {
    fontSize: "small",
    alignItems: "flex-start",
    wordWrap: 'break-word',
    whiteSpace: 'pre-line'
  },
  copyURL: {
    marginTop: '4px',
    "&:disabled": {
      color: "grey"
    }
  },
  settings: {
    paddingRight: '10px',
    paddingTop: '10px',
    color: '#ffffff',
    "&:disabled": {
      color: "grey"
    }
  },
  hash: {
    fontWeight: 'bold'
  },
  searchBox: {
    paddingLeft: '6px',
    paddingRight: '5px',
    paddingBottom: '4px',
    width: '50%',
    ['@media (max-width: 1224px)']: {
      width: '95%'
    },
  },
  loginButtonWrapper: {
    marginLeft: '32px',
  },
  skillsWrapper: {
    width: '100%',
  },
  strikeButton: {
    height: '20px',
    width: '20px',
    minWidth: '32px',
    minHeight: '32px',
    borderRadius: '0px',
    marginBottom: '3px'
  },
  whatsAppButton: {
    height: '20px',
    width: '20px',
    minWidth: '32px',
    minHeight: '32px',
    borderRadius: '0px',
    marginBottom: '3px',
    color: 'white',
    backgroundColor: '#23d366'
  },
  refreshButton: {
    height: '20px',
    width: '20px',
    minWidth: '32px',
    minHeight: '32px',
    borderRadius: '0px',
    marginBottom: '3px'
  },
  paper: {
    border: '1px solid',
    padding: '1px',
    height: '100px',
    overflow: 'scroll'
  },
  hashButton: {
    textTransform: 'none',
    justifyContent: 'flex-start'
  }
});

const steps = [
  {
    selector: '[data-tut="tourButton"]',
    content: 'Welcome to Roughbook! 🥳 Know Roughbook better in this quick tour 🚂. You can retake this tour using this icon to explore all cool features.'
  },
  {
    selector: '[data-tut="loginWrapper"]',
    content: 'Once you Login or Register here 👶, all your notes will be available on your multiple devices.',
  },
  {
    selector: '[data-tut="saveNotes"]',
    content: 'Quickly add note or current tab URL 👻. Configure this to take enter for save.',
  },
  // {
  //   selector: '[data-tut="setReminder"]',
  //   content: 'Set Reminder 🐔 to get notification for next meeting or task.',
  // },
  {
    selector: '[data-tut="oldNotes"]',
    content: 'See all your notes 🦕 here. You can select old notes for further actions.',
  },
  {
    selector: '[data-tut="actionPanel"]',
    content: 'Action panel for selected notes. Strike-out ⚡️, organise #, send 📤 or search old notes 🔍.',
  },
  {
    selector: '[data-tut="settings"]',
    content: 'Restore deleted notes 🙈 or ask any new feature 🎉.'
  },
  {
    position: 'center',
    content: 'Tour Done!! Lets login and start adding your thoughts with hashtags(#) 📚.',
  },
];

const App = () => {
  const classes = useStyles();
  const [isTourOpen, setIsTourOpen] = React.useState(localStorage.getItem('roughbook_tourDone') !== 'true');
  const [state, dispatch] = useContext(AppContext);
  const [loginDialogOpen, setLoginDialogOpen] = React.useState(false);
  const [editNotesDialogOpen, setEditNotesDialogOpen] = React.useState(false);
  const [historyDialogOpen, setHistoryDialogOpen] = React.useState(false);
  const [slackDialogOpen, setSlackDialogOpen] = React.useState(false);
  const [featureDialogOpen, setFeatureDialogOpen] = React.useState(false);
  const [hashtagsDialogOpen, setHashtagsDialogOpen] = React.useState(false);
  const [linkDialogOpen, setLinkDialogOpen] = React.useState(false);
  const [trashDialogOpen, setTrashDialogOpen] = React.useState(false);
  const [recordDialogOpen, setRecordDialogOpen] = React.useState(false);
  const [option, setOption] = React.useState('Login');
  const [drawer, setDrawer] = React.useState(false);
  const [confirmPassword, setConfirmPassword] = React.useState('');
  const messagesEndRef = useRef(null)
  const [visibility, setVisibility] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [record, setRecord] = React.useState(false);
  const [copyTabPreference, setCopyTabPreference] = React.useState('currentTab');
  const web = true;

  let theme = createMuiTheme({
    typography: {
      h6: {
        fontSize: '1rem',
        '@media (min-width:300px)': {
          fontSize: '1.15rem',
        }
      },
      body2: {
        fontSize: '0.6rem',
        '@media (min-width:300px)': {
          fontSize: '0.8rem',
        }
      },
      subtitle1: {
        fontSize: '0.7rem',
        fontWeight: 'bold',
        '@media (min-width:300px)': {
          fontSize: '0.9rem',
        }
      }
    },
    palette: {
      type: state.isDark ? 'dark' : 'light',
      primary: {
        main: '#b2ff59',
        contrastText: '#282c34',
      },
      secondary: {
        main: '#1a92dc',
        contrastText: '#fff',
      },
      error: {
        main: '#ff5252'
      }
    }
  });

  const toggleLinkDialog = () => {
    setLinkDialogOpen(!linkDialogOpen);
  }

  const scrollToBottom = () => {
    if (messagesEndRef.current != null) {
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }

  const arrangeOldNotes = () => {
    if (Object.keys(state.oldNotes).length !== 0) {
      dispatch({ type: "ARRANGE_OLD_NOTES" });
    }
  }

  const arrangedNotesLength = Object.keys(state.arrangedNotes).length;
  const oldNotesLength = Object.keys(state.oldNotes).length;
  
  useEffect(arrangeOldNotes, [state.oldNotes]);
  useEffect(scrollToBottom, [arrangedNotesLength, oldNotesLength]);

  const handleLoginDialogClose = () => {
    setLoginDialogOpen(false);
  };

  const handleLoginDialogOpen = (optionSelected) => {
    setLoginDialogOpen(true);
    setOption(optionSelected);
  };

  const [expanded, setExpanded] = React.useState("panel1");

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
    if (panel === 'panel2') {
      dispatch({ type: "UPDATE_REMINDER_DATE_TIME", payload: formatDate(new Date(), "yyyy-mm-ddThh:MM") });
    } else {
      dispatch({ type: "UPDATE_REMINDER_DATE_TIME", payload: undefined });
    }
  };

  if (state.email === "" && localStorage.getItem('roughBook_email') !== null) {
    dispatch({ type: "UPDATE_FROM_LOCAL_STORAGE" });
  }

  let allNotesMessage = "Roughbook Notes from " + state.username + '\n\n';
  if (state.selectedNote !== {}) {
    Object.entries(state.selectedNote).forEach(([timestamp, selected]) => {
      if (selected) {
        const unlinkify = unLinkifyStatement(state.oldNotes[timestamp].statement);
        const unhyperlinking = unhyperlinkStatement(unlinkify);
        const unhashify = unhashifyStatement(unhyperlinking);
        const unspanify = unSpanifyStatement(unhashify);
        allNotesMessage += '\n' + unspanify;
      }
    });

    allNotesMessage += '\n\n'
      + 'Download your Roughbook Chrome Extension from '
      + 'https://chrome.google.com/webstore/detail/roughbook-quick-notes-tak/phomkpbjipcgmfpffhkaphddmclefofh'
  }

  const appendHashNameToNewNote = (hashName) => {
    if (state.newNotes === null) {
      dispatch({ type: "ADD_NEW_NOTES", payload: hashName + " " });
    } else {
      dispatch({ type: "ADD_NEW_NOTES", payload: state.newNotes + " " + hashName + " " });
    }

    setAnchorEl(null);
    if (document.getElementById("new-notes") !== null) {
      document.getElementById("new-notes").focus();
    }
  }

  const copyTabURLForLink = () => {
    chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
      const url = tabs[0].url;
      dispatch({ type: "UPDATE_LINK_URL", payload: url });
    });
  }

  const copyTabURLToEditNote = () => {
    chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
      const url = tabs[0].url;

      if (state.editNotes === undefined || state.editNotes.statement === undefined) {
        dispatch({ type: "ADD_TO_EDIT_NOTES", payload: url });
      } else {
        dispatch({ type: "ADD_TO_EDIT_NOTES", payload: state.editNotes.statement + " " + url });
      }
    });

    if (document.getElementById("edit-note") !== null) {
      document.getElementById("edit-note").focus();
    }
  }

  const copyTabURL = () => {
    if (copyTabPreference === 'currentTab') {
      chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
        const url = tabs[0].url;

        if (state.newNotes === null) {
          dispatch({ type: "ADD_NEW_NOTES", payload: url });
        } else {
          dispatch({ type: "ADD_NEW_NOTES", payload: state.newNotes + " " + url });
        }
      });
    } else if (copyTabPreference === 'allTabs') {
      chrome.windows.getCurrent({ populate: true, windowTypes: ['normal'] }, (window) => {
        if (window.tabs !== undefined && window.tabs.length > 0) {
          let note = 'All tabs:';
          window.tabs.forEach((tab, index) => {
            note += '\n' + (index + 1) + '. ' + tab.title + ', ' + tab.url;
          });
          if (state.newNotes === null) {
            dispatch({ type: "ADD_NEW_NOTES", payload: note });
          } else {
            dispatch({ type: "ADD_NEW_NOTES", payload: state.newNotes + "\n" + note });
          }
        }
      });
    }

    if (document.getElementById("new-notes") !== null) {
      document.getElementById("new-notes").focus();
    }
  }

  const shareNotesOnSlack = () => {
    navigator.clipboard.writeText(allNotesMessage);
    setSlackDialogOpen(true);
  }

  const processAudio = (recordedBlob) => {
    if (recordDialogOpen) {
      dispatch({ type: 'UPDATE_AUDIO', payload: recordedBlob });
    }
  }

  const saveAudio = () => {
    setRecordDialogOpen(false);
    dispatch({ type: "SAVE_AUDIO" });
  }

  const cancelRecording = () => {
    setRecordDialogOpen(false);
    setRecord(false);
    dispatch({ type: 'EMPTY_AUDIO_BLOB' });
  }

  const editNotes = (statement, timestamp) => {
    const unlinkify = unLinkifyStatement(statement);
    const unhyperlinking = unhyperlinkStatement(unlinkify);
    const unhashify = unhashifyStatement(unhyperlinking);
    const unspanify = unSpanifyStatement(unhashify);

    dispatch({ type: 'SAVE_NOTE_TO_EDIT', payload: { statement: unspanify, timestamp } });
    setEditNotesDialogOpen(true);
  }

  return (
    <ThemeProvider theme={theme}>
      <Paper elevation={0} square className={classes.ExtensionHeader}>
        <Grid container direction='row' justify="space-between" alignItems="center">
          <Grid item>
            <Grid container direction='row' justify="flex-start" alignItems="center">
                <img alt="pen" className={classes.ExtensionLogo} src="./logo48.png" height='30px' width='30px' />
                <Typography variant="h6" className={classes.title}>Roughbook</Typography>
                {state.loading && <CircularProgress size={20} />}
            </Grid>
          </Grid>
          <Grid item>
            <Grid container direction='row'>
              <Tooltip title="Color Theme">
                <IconButton
                  className={classes.settings}
                  size="small"
                  onClick={() => dispatch({ type: 'UPDATE_IS_DARK', payload: !state.isDark })}>
                  <Brightness4Icon fontSize='small' />
                </IconButton>
              </Tooltip>
              <Tooltip title="Settings">
                <IconButton
                  data-tut="settings"
                  disabled={state.email.trim().length === 0}
                  className={classes.settings}
                  size="small"
                  onClick={() => setDrawer(true)}>
                  <SettingsIcon fontSize='small' />
                </IconButton>
              </Tooltip>
              <Tooltip title="Roughbook Tour">
                <IconButton
                  data-tut="tourButton"
                  className={classes.settings}
                  size="small"
                  onClick={() => { setIsTourOpen(true); dispatch({ type: 'START_TOUR' }); }}>
                  <NotListedLocationIcon fontSize='small' />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Grid>
        {state.email.trim().length > 0 ?
          <Typography data-tut="loginWrapper" variant="body2" className={classes.email}>{state.email.toLowerCase()}</Typography>
          :
          <Box data-tut="loginWrapper" className={classes.loginButtonWrapper}>
            <Button size="small" className={classes.register} onClick={() => handleLoginDialogOpen('Login')}>
              Login
            </Button>
            <Button size="small" className={classes.register} onClick={() => handleLoginDialogOpen('Register')}>
              Register
            </Button>
          </Box>
        }
      </Paper>
        
      <Paper square className={state.loading && classes.Disabled}>
        <Paper square className={web ? classes.App : classes.AppExtension}>
          <Paper
            data-tut="actionPanel"
            variant="outlined"
            square
            elevation={2}
          >
            <Grid alignItems="center" container direction='row' justify='flex-start'>
              <Tooltip title={state.allNotesChecked ? "Unselect All" : "Select All"}>
                <div>
                  <Checkbox
                    checked={state.allNotesChecked}
                    onChange={(event) => dispatch({ type: 'ALL_NOTES_CHECK_TOGGLED', payload: event.target.checked })}
                    size='small'
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                  />
                </div>
              </Tooltip>

              <Tooltip title="Delete Notes">
                <div>
                  <Button
                    disabled={Object.entries(state.selectedNote).every(([timestamp, isSelected]) => !isSelected)}
                    color="primary"
                    disableElevation
                    variant="contained"
                    className={classes.strikeButton}
                    onClick={() => dispatch({ type: 'DELETE_NOTES' })}
                  >
                    <DeleteIcon />
                  </Button>
                </div>
              </Tooltip>
              <Tooltip title="Add Hashtags">
                <div>
                  <Button
                    disabled={Object.entries(state.selectedNote).every(([timestamp, isSelected]) => !isSelected)}
                    color="secondary"
                    disableElevation
                    variant="contained"
                    className={classes.strikeButton}
                    onClick={() => setHashtagsDialogOpen(true)}
                  >
                    <Typography variant="h6">#</Typography>
                  </Button>
                </div>
              </Tooltip>
              <Tooltip title="Email Selected Notes">
                <div>
                  <EmailShareButton
                    disabled={state.email.trim().length === 0}
                    subject="Roughbook Notes"
                    body={allNotesMessage}
                    url=""
                  >
                    <EmailIcon
                      size='2rem'
                    />
                  </EmailShareButton>
                </div>
              </Tooltip>
              <Tooltip title="WhatsApp Selected Notes">
                <div>
                  <Button
                    disabled={state.email.trim().length === 0}
                    disableElevation
                    variant="contained"
                    className={classes.whatsAppButton}
                    onClick={() => window.open('https://api.whatsapp.com/send?text=' + encodeURIComponent(allNotesMessage), '_blank')}
                  >
                    <WhatsAppIcon />
                  </Button>
                </div>
              </Tooltip>
              <Tooltip title="Slack Selected Notes">
                <div>
                  <Button
                    disabled={state.email.trim().length === 0}
                    disableElevation
                    variant="contained"
                    className={classes.strikeButton}
                    onClick={shareNotesOnSlack}
                  >
                    <img
                      alt="slack" src={state.email.trim().length === 0 ? slackDisabled : slack}
                    />
                  </Button>
                </div>
              </Tooltip>
              <Tooltip title="Refetch Notes">
                <div>
                  <Button
                    disabled={state.email.trim().length === 0}
                    disableElevation
                    variant="contained"
                    className={classes.refreshButton}
                    onClick={() => dispatch({ type: 'REFETCH_NOTES' })}
                  >
                    <RefreshIcon color="#ffffff" />
                  </Button>
                </div>
              </Tooltip>
              <Autocomplete
                size='small'
                fontSize='small'
                multiple
                getOptionDisabled={option => (option === '--------------------' || option === 'recently added...')}
                className={classes.searchBox}
                def
                options={state.recentlyUsedHashtags.length > 0
                  ? ['recently added...'].concat(state.recentlyUsedHashtags, ['--------------------'], Object.keys(state.hashMap)
                    .sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase())))
                  : Object.keys(state.hashMap).sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()))
                }
                id="search"
                value={state.toSearchHash}
                onChange={(event, newValue) => {
                  dispatch({ type: "UPDATE_SEARCH_HASH", payload: newValue });
                  if (newValue.length === 0) {
                    dispatch({ type: "LOAD_ALL_NOTES" });
                  }
                  else {
                    dispatch({ type: "LOAD_MATCHED_NOTES", payload: newValue });
                  }
                }}
                getOptionLabel={(option) => option}
                renderInput={(params) => <TextField {...params} InputLabelProps={{ style: { fontSize: 12 } }} size='small' label="Search by hashtags(#)" />}
              />
            </Grid>
          </Paper>

          <Paper
            data-tut="oldNotes"
            variant="elevation"
            square
            elevation={2}
            className={classes.OldNotesWrapper}
          >
            {state.email.trim().length === 0 ?
              <Typography variant="body2" color="error">
                <Box fontWeight="fontWeightBold" m={1}>
                  {state.error}
                </Box>
              </Typography> :
              Object.keys(state.arrangedNotes).length === 0 ?
                <p className={classes.notes}>Your roughbook is empty!<br /><br /> Start writing notes 🖋 with hashtags to get most arranged roughbook 📚.<br /> For example, <span className={classes.hash}>#call</span> tim-tim @ 4. Or <span className={classes.hash}>#importantUrl</span> https://www.w3schools.com/css/ <br /><br />You can strikeout notes ⚡️, tag notes 🔖, search by hashtags 📑 or even send your selected notes by email or whatsApp 📤.</p> :
                <Grid container direction="column" justify="flex-start"
                  alignItems="stretch">
                  {Object.entries(state.arrangedNotes).map(([createdOn, notesObject]) => {
                    if (state.allStrikedNotes[createdOn]) {
                      return <div></div>;
                    }
                    return <Grid item key={createdOn} xs={12}>
                      <p className={classes.dateTime}>{createdOn}</p>
                      {notesObject.map(({ timestamp, notes }) => {
                        let notesToDisplay = notes.statement;

                        if (notes.striked) {
                          return <div></div>;
                        }

                        return <Grid
                          container
                          key={timestamp}
                          direction="row"
                          justify="space-between"
                          alignItems="center"
                        >
                          <Checkbox
                            checked={state.selectedNote[timestamp]}
                            onChange={(event) => dispatch({ type: 'NOTE_CHECK_TOGGLED', payload: { checked: event.target.checked, timestamp } })}
                            size='small'
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                          />

                          { state.oldNotes[timestamp].audioS3
                            && state.audioURLs[timestamp] === undefined
                            && <Button
                              size="small"
                              color="secondary"
                              variant="outlined"
                              onClick={() => {
                                dispatch({
                                  type: 'DOWNLOAD_AUDIO',
                                  payload: {
                                    audioS3: state.oldNotes[timestamp].audioS3,
                                    timestamp
                                  }
                                })
                              }}
                              startIcon={<PlayForWorkIcon />
                              }
                            >
                              <Typography variant="body2">Prepare Audio to Play</Typography>
                            </Button>
                          }

                          {state.oldNotes[timestamp].audioS3
                            && state.audioURLs[timestamp] === 'downloading'
                            && <Typography variant="info">Loading...</Typography>
                          }

                          { state.audioURLs[timestamp]
                            && state.audioURLs[timestamp] !== 'downloading'
                            &&
                            <ReactAudioPlayer
                              src={state.audioURLs[timestamp]}
                              controls
                            />
                          }
                          <Grid item xs={9} >
                            <div className={classes.notes} dangerouslySetInnerHTML={{ __html: notesToDisplay }} />
                          </Grid>
                          <Grid item >
                              <Tooltip title="Edit Note">
                                <IconButton
                                  size="small"
                                  onClick={() => editNotes(notes.statement, timestamp)}>
                                  <EditIcon fontSize='small' />
                                </IconButton>
                              </Tooltip>
                            </Grid>
                            <Grid item >
                              <Tooltip title="Open Note History">
                                <IconButton
                                  disabled = {notes.history === undefined}
                                  size="small"
                                  onClick={() => {
                                    setHistoryDialogOpen(true); 
                                    dispatch({ type: 'CHECK_HISTORY', payload: notes.history });
                                  }}>
                                  <AccessTimeIcon fontSize='small' />
                                </IconButton>
                              </Tooltip>
                            </Grid>
                        </Grid>
                      })
                      }
                      <hr />
                    </Grid>

                  })}
                  <div ref={messagesEndRef} />
                </Grid>
            }
          </Paper>

          {!web && <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2bh-content"
              id="panel2bh-header"
              data-tut="setReminder"
            >
              <Typography variant="subtitle1">Set Reminder ⏰</Typography>
            </AccordionSummary>
            <AccordionDetails data-tut="default">
              <Box className={classes.skillsWrapper}>
                <TextField
                  disabled={state.email.trim().length === 0}
                  fullWidth
                  label="Notification Time"
                  type="datetime-local"
                  value={state.reminderDateTime}
                  defaultValue={formatDate(new Date(), "yyyy-mm-ddThh:MM")}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(event) => dispatch({ type: "UPDATE_REMINDER_DATE_TIME", payload: event.target.value })}
                />
                <Input
                  disabled={state.email.trim().length === 0}
                  multiline
                  fullWidth
                  value={state.reminderLabel}
                  rows={1}
                  rowsMax={2}
                  onKeyDown={(e) => {
                    if (state.enterToSaveReminder === true && e.key === "Enter") {
                      e.preventDefault();
                      dispatch({ type: "SAVE_NEW_REMINDER" });
                    }
                  }}
                  onChange={(event) => dispatch({ type: "UPDATE_NEW_REMINDER_LABEL", payload: event.target.value })}
                  placeholder="reminder label" >
                </Input>
                <Grid container direction="row" justify="space-between" alignItems="center">
                  <FormControlLabel
                    disabled={state.email.trim().length === 0}
                    size='small'
                    fontSize="small"
                    control={<Checkbox checked={state.enterToSaveReminder} onChange={(event) => dispatch({ type: 'USE_ENTER_TO_SAVE_REMINDER', payload: event.target.checked })} />}
                    label={<Typography variant="body2">Press Enter to save.</Typography>}
                  />

                  {!web &&
                    <a style={{ color: state.isDark ? '#b2ff59' : '#1a92dc' }} rel="noreferrer" target="_blank" href='https://roughbook.digital/'>Visit Roughbook Website</a>}
                  {!state.enterToSaveReminder && <Button
                    color='primary'
                    variant='contained'
                    disabled={state.email.trim().length === 0}
                    size="small"
                    className={classes.copyURL}
                    onClick={() => dispatch({ type: "SAVE_NEW_REMINDER" })}>Save</Button>}
                </Grid>
              </Box>
            </AccordionDetails>
          </Accordion>
          }

          <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography variant="subtitle1">Save Notes 📝</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box data-tut="saveNotes" className={classes.NewNotes}>
                <Grid container direction="row" justify="space-between" alignItems="center">
                  <Grid item>
                    <ClickAwayListener onClickAway={() => setAnchorEl(null)}>
                      <div>
                        <Button
                          disabled={state.email.trim().length === 0 || Object.keys(state.hashMap).length === 0}
                          color="secondary"
                          variant="contained"
                          className={classes.strikeButton}
                          onClick={(event) => setAnchorEl(anchorEl ? null : event.currentTarget)}
                        >
                          <Typography variant="h6">#</Typography>
                        </Button>
                        <Popper open={Boolean(anchorEl)} anchorEl={anchorEl}>
                          <Paper elevation={2} className={classes.paper} >
                            <Grid container direction='column'>
                              {state.recentlyUsedHashtags.length > 0 &&
                                <Grid container direction='column'>
                                  <Button disabled size='small' className={classes.hashButton} key="recent">recently added...</Button>
                                  {state.recentlyUsedHashtags
                                    .map((hashName) => {
                                      return <Button size='small' onClick={() => appendHashNameToNewNote(hashName)} className={classes.hashButton} key={hashName}>{hashName}</Button>
                                    })}
                                  <Button disabled size='small' className={classes.hashButton} key="dash">--------------------</Button>
                                </Grid>}
                              {Object.keys(state.hashMap)
                                .sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()))
                                .map((hashName) => {
                                  return <Button onClick={() => appendHashNameToNewNote(hashName)} className={classes.hashButton} key={hashName}>{hashName}</Button>
                                })}
                            </Grid>
                          </Paper>
                        </Popper>
                      </div>
                    </ClickAwayListener>
                  </Grid>
                  <Grid item xs={9}>
                    <Input
                      id="new-notes"
                      disabled={state.email.trim().length === 0}
                      multiline
                      autoFocus
                      fullWidth
                      value={state.newNotes}
                      rows={1}
                      rowsMax={4}
                      onKeyDown={(e) => {
                        if (state.enterToSaveNotes === true && e.key === "Enter") {
                          e.preventDefault();
                          dispatch({ type: "SAVE_NEW_NOTES" });
                        }
                      }}
                      onChange={(event) => dispatch({ type: "ADD_NEW_NOTES", payload: event.target.value })}
                      placeholder="new notes or thoughts" >
                    </Input>
                  </Grid>
                  <Grid item>
                    <Tooltip title="Record Audio">
                      <Button
                        disabled={state.email.trim().length === 0}
                        color="secondary"
                        variant="contained"
                        className={classes.strikeButton}
                        onClick={() => {
                          dispatch({ type: "CHECK_AUDIO_ACCESS" });
                          dispatch({ type: "EMPTY_AUDIO_BLOB" });
                          setRecordDialogOpen(true);
                        }}
                      >
                        <MicIcon />
                      </Button>
                    </Tooltip>
                  </Grid>
                  <Grid item>
                    <Tooltip title="Insert Link">
                      <Button
                        disabled={state.email.trim().length === 0}
                        color="primary"
                        variant="contained"
                        className={classes.strikeButton}
                        onClick={toggleLinkDialog}
                      >
                        <LinkIcon />
                      </Button>
                    </Tooltip>
                  </Grid>
                </Grid>
                <Grid container direction="row" justify="space-between" alignItems="center">
                  <FormControlLabel
                    disabled={state.email.trim().length === 0}
                    size='small'
                    fontSize="small"
                    control={<Checkbox checked={state.enterToSaveNotes} onChange={(event) => dispatch({ type: 'USE_ENTER_TO_SAVE_NOTE', payload: event.target.checked })} />}
                    label={<Typography variant="body2">Press Enter to save.</Typography>}
                  />
                  {!web
                    &&
                    <a style={{ color: state.isDark ? '#b2ff59' : '#1a92dc' }} rel="noreferrer" target="_blank" href='https://roughbook.digital/'>Visit Roughbook Website</a>}
                  {!state.enterToSaveNotes && <Button color='primary' variant='contained' disabled={state.email.trim().length === 0} size="small" className={classes.copyURL} onClick={() => dispatch({ type: "SAVE_NEW_NOTES" })}>Save</Button>}
                </Grid>
                {!web &&
                  <Grid spacing={1} container direction="row" justify="start" alignItems="center">
                    <Grid item>
                      <Button color='primary' variant='contained' disabled={state.email.trim().length === 0} size='small' className={classes.copyURL} onClick={() => copyTabURL()}>Add Tab URL</Button>
                    </Grid>
                    <Grid item>
                      <RadioGroup row name="copyTab" value={copyTabPreference} onChange={(event) => setCopyTabPreference(event.target.value)}>

                        <FormControlLabel
                          value="currentTab"
                          size='small'
                          fontSize="small"
                          control={<Radio />}
                          label={<Typography variant="body2">Current Tab URL</Typography>}
                        />
                        <FormControlLabel
                          value="allTabs"
                          size='small'
                          fontSize="small"
                          control={<Radio />}
                          label={<Typography variant="body2">All Tabs URLs</Typography>}
                        />
                      </RadioGroup>
                    </Grid>
                  </Grid>}
              </Box>
            </AccordionDetails>
          </Accordion>

          <Drawer anchor={'right'} open={drawer} onClose={() => setDrawer(false)}>
            <Grid container direction='column' justify='flex-start' alignItems='flex-start' className={classes.SettingsWrapper}>
              <Button
                onClick={() => { setDrawer(false); setFeatureDialogOpen(true) }}
                color='primary'
                startIcon={<AddCircleIcon />}>
                Ask New Feature
              </Button>
              <hr />
              <Button
                onClick={() => { setDrawer(false); setTrashDialogOpen(true) }}
                color='primary'
                startIcon={<DeleteIcon />}>
                Check Deleted Notes
              </Button>
              <hr />
              <Button
                onClick={() => { dispatch({ type: 'SIGN_OUT' }); setDrawer(false) }}
                color='primary'
                startIcon={<ExitToAppIcon />}>
                Sign Out
          </Button>
            </Grid>
          </Drawer>

          <Dialog
            open={trashDialogOpen}
            onClose={() => setTrashDialogOpen(false)}
            fullWidth
            aria-labelledby="form-dialog-edit"
          >
            <DialogTitle>{"Deleted Notes"}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                {(state.deletedNotes !== undefined 
                && state.deletedNotesCount > 0)
                ? <Typography
                    variant='subtitle2'>
                    {state.deletedNotesCount} Deleted Notes. Restore will save note to the time when it was first wrote.
                  </Typography>
                : <Typography
                    variant='subtitle2'>
                    0 Deleted Notes.
                  </Typography>}
              </DialogContentText>
            </DialogContent>
            <Paper 
                 className={classes.DeletedNotesWrapper}>
                  {state.deletedNotes !== undefined && 
                  Object.entries(state.deletedNotes).map(([convertedDate, deletedNoteObject]) => 
                  { return <Paper elevation={0} 
                  className={state.loading && classes.Disabled}>
                      <p className={classes.dateTime}>{convertedDate}</p>
                      {deletedNoteObject.map((notesObject) => {
                        return <Paper elevation={0}>
                          <Grid container direction="row" justify="space-between">
                            <Grid item xs={10}>
                              <div className={classes.notes} dangerouslySetInnerHTML={{ __html: notesObject.notes.statement }} />
                            </Grid>
                            <Grid item>
                            <Tooltip title="Restore Note">
                              <Button
                                onClick={() => dispatch({ type: "RESTORE_DELETED_NOTE", payload: notesObject.timestamp})}>
                                <RefreshIcon />
                              </Button>
                              </Tooltip>
                            </Grid>
                          </Grid>
                          </Paper>
                      })}
                      <hr/>
                    </Paper> })
                  }
            </Paper>
            
            <DialogActions>
            <Grid container direction="row" justify='space-between'>
                <Grid item>
                  <Button
                    variant='outlined'
                    size='small'
                    color="primary"
                    onClick={() => setTrashDialogOpen(false)}>
                    Close
                  </Button>
                </Grid>
              </Grid>
            </DialogActions>
          </Dialog>

          <Dialog
            open={historyDialogOpen}
            onClose={() => setHistoryDialogOpen(false)}
            fullWidth
            aria-labelledby="form-dialog-edit"
          >
            <DialogTitle>{"Note History"}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Note was changed{state.history && <span> {state.history.length} times</span>}. Following is the note's journey, oldest to newest.
              </DialogContentText>
            </DialogContent>
            <Paper 
                className={classes.HistoryWrapper}>
            { state.history && state.history.map((historyEntry) => 
              <Paper elevation={0}>
                <Grid container direction="column" alignItems="flex-start" key={historyEntry.timestamp}>
                <Grid item>
                <p className={classes.dateTime}>{convertStampDate(historyEntry.timestamp)}</p>
                </Grid>
                <Grid item>
                  <div className={classes.notes} dangerouslySetInnerHTML={{ __html: historyEntry.statement }} />
                </Grid>
              </Grid>
              <hr />
              </Paper>
            )}
            </Paper>
            <DialogActions>
              <Button
                variant='outlined'
                size='small'
                color="primary"
                onClick={() => setHistoryDialogOpen(false)}>
                Close
          </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={editNotesDialogOpen}
            onClose={() => setEditNotesDialogOpen(false)}
            fullWidth
            aria-labelledby="form-dialog-edit"
          >
            <DialogContent>
              <DialogContentText>
                Edit Note
              </DialogContentText>
            </DialogContent>
              <Input
                id='edit-note'
                size='small'
                fontSize='small'
                className={classes.dialogInputText}
                autoFocus
                multiline
                rowsMax={20}
                value={state.editNotes ? state.editNotes.statement : ""}
                onChange={(event) => dispatch({ type: "SAVE_NOTE_TO_EDIT", payload: { statement: event.target.value } })} 
                onFocus={(event)=> event.currentTarget.setSelectionRange(event.currentTarget.value.length, event.currentTarget.value.length)}
              />
            <DialogActions>
            <Grid container direction="column">
            {state.editNotes !== undefined
                    && state.editNotes.statement !== undefined
                    && state.editNotes.statement.length === 0 
                    && <Typography variant="body2" color="error">Can't remove the whole note. Instead use select and delete notes.</Typography>}
              <Grid container direction="row" justify='space-between'>
                <Grid item>
                  <Button
                    variant='contained'
                    size='small'
                    color="primary"
                    onClick={copyTabURLToEditNote}>
                    Copy tab URL
                  </Button>
                </Grid>
                <Grid item>
                  <Grid container direction="row" spacing={1}>
                    <Grid item>
                      <Button
                        variant='contained'
                        size='small'
                        color="primary"
                        disabled={state.editNotes !== undefined && state.editNotes.statement !== undefined && state.editNotes.statement.length === 0}
                        onClick={() => {
                          dispatch({ type: "SAVE_EDITED_NOTE" });
                          setEditNotesDialogOpen(false);
                        }}>
                        Save
                      </Button>
                    </Grid>
                    <Grid item>
                      <Button
                        variant='outlined'
                        size='small'
                        color="primary"
                        onClick={() => setEditNotesDialogOpen(false)}>
                        Cancel
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
              
            </DialogActions>
          </Dialog>

          <Dialog
            open={loginDialogOpen}
            onClose={handleLoginDialogClose}
            fullWidth
            aria-labelledby="form-dialog-title"
          >
            <DialogContent>
              <DialogContentText>
                Thanks for choosing Roughbook!!
            </DialogContentText>
            </DialogContent>
            <Input
              size='small'
              fontSize='small'
              type="email"
              placeholder="Email"
              className={classes.dialogInputText}
              autoFocus
              value={state.signInForm ? state.signInForm.email : ""}
              onChange={(event) => dispatch({ type: "UPDATE_SIGN_IN_FORM_EMAIL", payload: event.target.value })} />
            {option === 'Register' &&
              <Input
                size='small'
                fontSize='small'
                placeholder="Your Name"
                className={classes.dialogInputText}
                value={state.signInForm ? state.signInForm.username : ""}
                onChange={(event) => dispatch({ type: "UPDATE_SIGN_IN_FORM_USERNAME", payload: event.target.value })} />}
            <Grid container direction='row' justify='space-between'>
              <Input
                size='small'
                fontSize='small'
                type={visibility ? "text" : "password"}
                placeholder="Password"
                className={classes.dialogInputText}
                value={state.signInForm ? state.signInForm.passcode : ""}
                onChange={(event) => dispatch({ type: "UPDATE_SIGN_IN_FORM_PASSCODE", payload: event.target.value })} />

              {visibility ?
                <IconButton size='small' color="primary" onClick={() => setVisibility(false)}><VisibilityIcon /></IconButton> :
                <IconButton size='small' onClick={() => setVisibility(true)}><VisibilityOffIcon /></IconButton>
              }
            </Grid>
            {option === 'Register' &&
              <Input
                size='small'
                fontSize='small'
                name="confirmPassword"
                type="password"
                placeholder="Confirm Password"
                className={classes.dialogInputText}
                value={confirmPassword}
                onChange={(event) => setConfirmPassword(event.target.value)} />
            }

            <DialogActions>
              {(state.signInForm.email && !isValidEmailId(state.signInForm.email)) ?
                <Typography variant="body2" color="error">Provide valid Email Id.</Typography> :
                ((option === 'Register' && state.signInForm !== {} && confirmPassword.length !== 0 && confirmPassword !== state.signInForm.passcode) ?
                  <Typography variant="body2" color="error">Password and Confirm Password are not matching.</Typography> :
                  <br />)
              }
              <Button
                variant='contained'
                size='small'
                disabled={!isValidEmailId(state.signInForm.email) || state.signInForm.passcode === undefined || state.signInForm.passcode.trim().length === 0 || (option === 'Register' && confirmPassword !== state.signInForm.passcode)}
                color="primary"
                onClick={() => { dispatch({ type: "SIGN_IN", payload: option }); handleLoginDialogClose(); }}>
                {option}
              </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={featureDialogOpen}
            onClose={() => setFeatureDialogOpen(false)}
            fullWidth
            aria-labelledby="form-dialog-title"
          >
            <DialogContent>
              <DialogContentText>
                Thanks for letting us know about the new feature you want in Roughbook!! We will let you know the status by emails.
              </DialogContentText>
            </DialogContent>
            <Input
              size='small'
              fontSize='small'
              autoFocus
              multiline
              rows={1}
              rowsMax={4}
              placeholder="New Feature"
              className={classes.dialogInputText}
              value={state.newFeature}
              onChange={(event) => dispatch({ type: "UPDATE_NEW_FEATURE", payload: event.target.value })} />
            <DialogActions>
              <Button
                variant='contained'
                size='small'
                disabled={state.newFeature === undefined || state.newFeature.trim().length === 0}
                color="primary"
                onClick={() => { dispatch({ type: "ASK_NEW_FEATURE" }); setFeatureDialogOpen(false); }}>
                Submit
          </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={hashtagsDialogOpen}
            onClose={() => setHashtagsDialogOpen(false)}
            fullWidth
            aria-labelledby="form-dialog-title"
          >
            <DialogContent>
              <DialogContentText>
                Add new hashtags to the selected notes. Can mention multiple hashtags separated with space.
      </DialogContentText>
            </DialogContent>
            <Input
              size='small'
              fontSize='small'
              autoFocus
              placeholder="hashtags like #importantLinks #blogs"
              className={classes.dialogInputText}
              value={state.newHashtags}
              onChange={(event) => dispatch({ type: "UPDATE_NEW_HASHTAGS", payload: event.target.value })} />
            <DialogActions>
              <Button
                variant='contained'
                size='small'
                disabled={state.newHashtags === undefined || state.newHashtags.trim().length === 0}
                color="primary"
                onClick={() => { dispatch({ type: "ADD_NEW_HASHTAGS" }); setHashtagsDialogOpen(false); }}>
                ADD
          </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={slackDialogOpen}
            onClose={() => setSlackDialogOpen(false)}
          >
            <DialogTitle>Notes Copied</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Your notes are copied to clipboard. Select slack channel where you wish to send the notes and directly paste (Ctrl+V) in the message box.
          </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                variant='contained'
                size='small'
                color="primary"
                autoFocus
                onClick={() => {
                  setSlackDialogOpen(false);
                  window.open('https://slack.com/app_redirect?channel=general', "_local");
                }}>
                OK, Proceed to Slack
          </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={linkDialogOpen}
            onClose={() => setLinkDialogOpen(false)}
            fullWidth
            aria-labelledby="form-dialog-title"
          >
            <DialogContent>
              <DialogContentText>
                Provide text and URL to insert link. The note will be saved as clickable text, redirecting to given URL.
          </DialogContentText>
            </DialogContent>
            <Input
              size='small'
              fontSize='small'
              autoFocus
              placeholder="text"
              className={classes.dialogInputText}
              value={state.linkText}
              onChange={(event) => dispatch({ type: "UPDATE_LINK_TEXT", payload: event.target.value })} />
            <Input
              size='small'
              fontSize='small'
              placeholder="URL"
              className={classes.dialogInputText}
              value={state.linkURL}
              onChange={(event) => dispatch({ type: "UPDATE_LINK_URL", payload: event.target.value })} />
            <DialogActions>
              <Grid container direction='row' justify='space-between'>
                <Button
                  variant='contained'
                  disabled={state.linkText.trim().length === 0}
                  size='small'
                  color="primary"
                  onClick={() => copyTabURLForLink()}>Add Current Tab URL
            </Button>
                <Button
                  variant='contained'
                  size='small'
                  disabled={state.linkText.trim().length === 0 || state.linkURL.trim().length === 0}
                  color="primary"
                  onClick={() => { dispatch({ type: "INSERT_LINK" }); setLinkDialogOpen(false); }}>
                  Insert
            </Button>
              </Grid>
            </DialogActions>
          </Dialog>

          <Dialog
            open={recordDialogOpen}
            onClose={() => setRecordDialogOpen(false)}
            fullWidth
          >
            <DialogContent>
              {(state.audioBlob !== undefined) ? <DialogContentText>
                Audio recorded. Save to proceed. Or Rerecord to overwrite this copy.
          </DialogContentText> :
                <DialogContentText>
                  Start recording audio.
          </DialogContentText>}
            </DialogContent>
            <ReactMic
              record={record}
              visualSetting="frequencyBars"
              onStop={(recordedBlob) => processAudio(recordedBlob)}
              onBlock={() => { setRecordDialogOpen(false); setRecord(false); dispatch({ type: 'EMPTY_AUDIO_BLOB' }); }}
              strokeColor='#b2ff59'
              backgroundColor='#282c34'
            />
            <DialogActions>
              <Tooltip title="Start Recording">
                <Button
                  variant='contained'
                  size='small'
                  disabled={record}
                  color="primary"
                  onClick={() => { setRecord(true); }}>
                  <FiberManualRecordIcon />
                </Button>
              </Tooltip>
              <Tooltip title="Stop Recording">
                <Button
                  variant='contained'
                  size='small'
                  disabled={!record}
                  color="primary"
                  onClick={() => { setRecord(false); }}>
                  <StopIcon />
                </Button>
              </Tooltip>
              <Button
                variant='contained'
                size='small'
                disabled={record || state.audioBlob === undefined}
                color="primary"
                onClick={saveAudio}>
                Save
          </Button>
              <Button
                variant='outlined'
                size='small'
                color="primary"
                onClick={cancelRecording}>
                Cancel
          </Button>
            </DialogActions>
          </Dialog>

          <Tour
            startAt={0}
            steps={steps}
            isOpen={isTourOpen}
            onRequestClose={() => setIsTourOpen(false)}
            disableInteraction
            rounded={4}
            onBeforeClose={() => dispatch({ type: 'CLOSE_TOUR' })}
          />
        </Paper>
      </Paper>

      {web &&
        <Paper square className={classes.Footer}>
          <a style={{ color: '#b2ff59' }} rel="noreferrer" target="_blank" href='https://chrome.google.com/webstore/detail/roughbook-quick-notes-tak/phomkpbjipcgmfpffhkaphddmclefofh'>Get Roughbook Browser Extension</a>
          <br />
          <Typography variant="subtitle2" className={classes.title} color='#ffffff'>Don't break your flow of work to add notes.</Typography>
        </Paper>}
    </ThemeProvider>
  );
}

const convertStampDate = (timestamp) => {
  var months_arr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  var date = new Date(parseInt(timestamp));
  var year = date.getFullYear();
  var month = months_arr[date.getMonth()];
  var day = date.getDate();
  var hours = "0" + date.getHours();
  var minutes = "0" + date.getMinutes();

  var fulldate = month + ' ' + day + ' ' + year + ' ' + hours.substr(-2) + ':' + minutes.substr(-2);
  return fulldate;
}

function formatDate(date, format) {
  const map = {
    mm: ("0" + (date.getMonth() + 1)).substr(-2),
    dd: ("0" + date.getDate()).substr(-2),
    yyyy: date.getFullYear(),
    hh: ("0" + date.getHours()).substr(-2),
    MM: ("0" + date.getMinutes()).substr(-2)
  }

  return format.replace(/mm|dd|yyyy|hh|MM/gi, matched => map[matched])
}

const unhashifyStatement = (statement) => {
  return statement.replaceAll("<span style=\"color:rgb(26,146,220);font-weight:bold\">", "");
}

const unLinkifyStatement = (statement) => {
  return statement.replace(/(<a style="color:rgb\(26,146,220\);" target="_blank" href='.*'>)/gim, "").replace(/<a target="_blank" href='.*'>/gim, "")
}

const unhyperlinkStatement = (statement) => {
  const linkText = /(<a style="color:rgb\(26,175,220\);" target="_blank" href='(.*)'>(.*)<\/a>)/gim;
  return statement.replace(linkText, "$3($2)")
}

const unSpanifyStatement = (statement) => {
  return statement.replaceAll("</span>", "").replaceAll("</a>", "")
}

const isValidEmailId = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
export default App;

