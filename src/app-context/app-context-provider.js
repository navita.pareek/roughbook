import React, { createContext, useReducer } from 'react';
import AppReducer from './app-reducer';
import {AppMiddleware} from './app-middleware';

const initialState = {
    newNotes: "",
    oldNotes: {},
    email:"",
    passcode: "",
    signInForm: {},
    arrangedNotes: {},
    hashMap: {},
    allArrangedNotes: {},
    selectedNote: {},
    toSearchHash: [],
    recentlyUsedHashtags: [],
    reminderLabel: "",
    allNotesChecked: false,
    enterToSaveNotes: false,
    enterToSaveReminder: false,
    error: "Login/Register to proceed.",
    linkText: "",
    linkURL: "",
    isDark: true,
    audioURLs: {},
    loading: false,
    deletedNotesCount: 0
};

const AppContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(AppReducer, initialState);
    const middlerware = AppMiddleware(dispatch, state);

    return (
        <AppContext.Provider value={[state, middlerware, dispatch ]}>
            {children}
        </AppContext.Provider>
    );
};

export const AppContext = createContext(initialState);

export default AppContextProvider