/*global chrome*/
export const AppMiddleware = (dispatch, state) => (action) => {
    let actionList = [];
    switch (action.type) {
        case 'SIGN_IN':
            if (state.signInForm && state.signInForm.email && state.signInForm.passcode) {
                (async () => {
                    dispatch({ type: 'LOADING_START'});
                    const payload = {
                        member: {
                            username: state.signInForm.username,
                            email: state.signInForm.email.toLowerCase(),
                            passcode: state.signInForm.passcode,
                        },
                        option: action.payload,
                        action: "ADD_ROUGHBOOK_PROFILE"
                    }
                    try {
                        const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                            method: 'POST',
                            body: JSON.stringify(payload)
                        });

                        const responseJson = await response.json();
                        if (responseJson.statusCode === 200) {
                            localStorage.setItem('roughBook_email', state.signInForm.email);
                            localStorage.setItem('roughBook_password', state.signInForm.passcode);
                            localStorage.setItem('roughbook_oldNotes', JSON.stringify(responseJson.data.notes));
                            localStorage.setItem('roughbook_username', responseJson.data.username);
                            localStorage.setItem('roughBook_enterToSaveNotes', responseJson.data.enterToSaveNotes);
                            localStorage.setItem('roughBook_enterToSaveReminder', responseJson.data.enterToSaveReminder);
                            localStorage.setItem('roughbook_isDark', responseJson.data.isDark);
                            dispatch({ type: 'SIGN_IN_SUCCESS', payload: responseJson.data });
                        } else {
                            dispatch({ type: 'UPDATE_ERROR', payload: responseJson.errorMessage });
                        }
                    } catch(exception) {
                        dispatch({ type: 'UPDATE_ERROR', payload: "Unable to Login/Register as not able to connect to server :(" });
                    }
                    dispatch({ type: 'LOADING_STOP'});
                })();
            }
            break;
        case 'REFETCH_NOTES':
            (async () => {
                dispatch({ type: 'LOADING_START'});
                const payload = {
                    member: {
                        email: state.email.toLowerCase(),
                        passcode: state.passcode,
                    },
                    option: "Login",
                    action: "ADD_ROUGHBOOK_PROFILE"
                }

                const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                    method: 'POST',
                    body: JSON.stringify(payload)
                });

                const responseJson = await response.json();
                if (responseJson.statusCode === 200) {
                    localStorage.setItem('roughbook_oldNotes', JSON.stringify(responseJson.data.notes));
                    dispatch({ type: 'REFETCH_NOTES_SUCCESS', payload: responseJson.data });
                }
                dispatch({ type: 'LOADING_STOP'});
            })();
            break;
        case 'USE_ENTER_TO_SAVE_NOTE':
            localStorage.setItem('roughBook_enterToSaveNotes', action.payload);
            updateUserProfile(state.email, 'enterToSaveNotes', action.payload);
            dispatch({ type: 'USE_ENTER_TO_SAVE_NOTE', payload: action.payload })
            break;
        case 'USE_ENTER_TO_SAVE_REMINDER':
            localStorage.setItem('roughBook_enterToSaveReminder', action.payload);
            updateUserProfile(state.email, 'enterToSaveReminder', action.payload);
            dispatch({ type: 'USE_ENTER_TO_SAVE_REMINDER', payload: action.payload })
            break;
        case 'SAVE_EDITED_NOTE': 
            if(state.editNotes && state.editNotes.statement) {
                const timestamp = state.editNotes.timestamp;
                const oldNote = state.oldNotes[timestamp].statement;
                if(oldNote === state.editNotes.statement) {
                    break;
                }

                (async () => {
                    dispatch({ type: 'LOADING_START'});
                    const linkifiedNewNotes = linkify(state.editNotes.statement);
                    const hashifiedNewNotes = hashify(linkifiedNewNotes); 
                    const payload = {
                        timestamp, 
                        statement: hashifiedNewNotes,
                        email: state.email.toLowerCase(),
                        action: "SAVE_EDITED_NOTE"
                    }

                    const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                        method: 'POST',
                        body: JSON.stringify(payload)
                    });

                    const responseJson = await response.json();
                    if (responseJson.statusCode === 200) {
                        dispatch({ 
                            type: 'SAVE_EDITED_NOTE_SUCCESS', 
                            payload: {
                                timestamp, 
                                statement: hashifiedNewNotes
                            }
                        });

                        let oldNotes = JSON.parse(localStorage.getItem('roughbook_oldNotes'));
                        if(oldNotes[timestamp].history !== undefined) {
                            oldNotes[timestamp].history.push(
                                {   timestamp: Date.now(), 
                                    statement: hashifiedNewNotes
                                });
                        } else {
                            oldNotes[timestamp] = {
                                ...oldNotes[timestamp],
                                statement: hashifiedNewNotes,
                                history: [{
                                    timestamp, 
                                    statement: oldNote
                                }, 
                                {
                                    timestamp: Date.now(),
                                    statement: hashifiedNewNotes
                                }]
                            }
                        }
                        oldNotes[timestamp].statement = hashifiedNewNotes;
                        localStorage.setItem('roughbook_oldNotes', JSON.stringify(oldNotes));
                    }
                    dispatch({ type: 'LOADING_STOP'});
                })();
            }
            break;
        case 'SAVE_NEW_NOTES':
            if (state.newNotes.trim().length > 0) {
                (async () => {
                    dispatch({ type: 'LOADING_START'});
                    const currentDate = Date.now();
                    const linkifiedNewNotes = linkify(state.newNotes);
                    const hashifiedNewNotes = hashify(linkifiedNewNotes);

                    const payload = {
                        createdOn: currentDate,
                        email: state.email.toLowerCase(),
                        notes: { statement: hashifiedNewNotes, striked: false },
                        action: "ADD_NEW_NOTES"
                    }
                    const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                        method: 'POST',
                        body: JSON.stringify(payload)
                    });

                    const responseJson = await response.json();
                    if (responseJson.statusCode === 200) {
                        dispatch({ type: 'SAVE_NEW_NOTES_SUCCESS', payload: { currentDate, hashifiedNewNotes } });

                        let oldNotes = {
                            [currentDate]: { statement: hashifiedNewNotes, striked: false }
                        }
                        if (localStorage.getItem('roughbook_oldNotes') != null) {
                            oldNotes = {
                                ...JSON.parse(localStorage.getItem('roughbook_oldNotes')),
                                ...oldNotes
                            }
                        }
                        localStorage.setItem('roughbook_oldNotes', JSON.stringify(oldNotes));
                    }
                    dispatch({ type: 'LOADING_STOP'});
                })();
            }
            break;
        case 'ADD_NEW_HASHTAGS':
            if (state.newHashtags && state.newHashtags.trim().length > 0) {
                (async () => {
                    dispatch({ type: 'LOADING_START'});
                    const hashifiedNewHashTags = hashify(state.newHashtags);
                    actionList = [];
                    Object.entries(state.selectedNote).forEach(([timestamp, isSelected]) => {
                        if (isSelected) {
                            actionList.push({ timestamp, statement: hashifiedNewHashTags + ' ' + state.oldNotes[timestamp].statement });
                        }
                    });

                    if(actionList.length > 0) {
                        const payload = {
                            actionList: actionList,
                            email: state.email.toLowerCase(),
                            action: "ADD_NEW_HASHTAGS"
                        }
    
                        const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                            method: 'POST',
                            body: JSON.stringify(payload)
                        });
    
                        const responseJson = await response.json();
                        if (responseJson.statusCode === 200) {
                            dispatch({ type: 'ADD_NEW_HASHTAGS_SUCCESS', payload: actionList });
    
                            let oldNotes = JSON.parse(localStorage.getItem('roughbook_oldNotes'));
                            actionList.forEach((value) => {
                                oldNotes[value.timestamp] = {
                                    ...oldNotes[value.timestamp],
                                    statement: value.statement
                                }
                            });
                            localStorage.setItem('roughbook_oldNotes', JSON.stringify(oldNotes));
                        }
                    }
                    dispatch({ type: 'LOADING_STOP'});
                })();
            }
            break;
        case 'SIGN_OUT':
            localStorage.removeItem('roughbook_username');
            localStorage.removeItem('roughBook_email');
            localStorage.removeItem('roughBook_password');
            localStorage.removeItem('roughbook_oldNotes');
            localStorage.removeItem('roughBook_enterToSaveNotes');
            localStorage.removeItem('roughBook_enterToSaveReminder');
            localStorage.removeItem('roughBook_newNotes');
            localStorage.removeItem('roughbook_isDark');
            dispatch({ type: 'SIGN_OUT' });
            break;
        case 'RESTORE_DELETED_NOTE':
            (async () => {
                dispatch({ type: 'LOADING_START'});
                actionList = [];
                actionList.push({ timestamp: action.payload, striked: false });

                const payload = {
                    actionList: actionList,
                    email: state.email.toLowerCase(),
                    action: "TOGGLE_STRIKE_THROUGH"
                }

                const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                    method: 'POST',
                    body: JSON.stringify(payload)
                });

                const responseJson = await response.json();
                if (responseJson.statusCode === 200) {
                    let oldNotes = JSON.parse(localStorage.getItem('roughbook_oldNotes'));
                    actionList.forEach((value) => {
                        dispatch({ type: 'DELETE_NOTES_SUCCESS', payload: { createdOnDate: value.timestamp, newStrike: value.striked } });
                        oldNotes[value.timestamp] = {
                            ...oldNotes[value.timestamp],
                            striked: value.striked
                        }
                    });
                    localStorage.setItem('roughbook_oldNotes', JSON.stringify(oldNotes));
                    dispatch({ type: 'ALL_NOTES_CHECK_TOGGLED', payload: false })
                }
                dispatch({ type: 'LOADING_STOP'});
            })();
            break;
        case 'DELETE_NOTES':
            (async () => {
                dispatch({ type: 'LOADING_START'});
                actionList = [];
                Object.entries(state.selectedNote).forEach(([timestamp, isSelected]) => {
                    if (isSelected) {
                        actionList.push({ timestamp, striked: true });
                    }
                });

                const payload = {
                    actionList: actionList,
                    email: state.email.toLowerCase(),
                    action: "TOGGLE_STRIKE_THROUGH"
                }
                const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                    method: 'POST',
                    body: JSON.stringify(payload)
                });

                const responseJson = await response.json();
                if (responseJson.statusCode === 200) {
                    let oldNotes = JSON.parse(localStorage.getItem('roughbook_oldNotes'));
                    actionList.forEach((value) => {
                        dispatch({ type: 'DELETE_NOTES_SUCCESS', payload: { createdOnDate: value.timestamp, newStrike: value.striked } });
                        oldNotes[value.timestamp] = {
                            ...oldNotes[value.timestamp],
                            striked: value.striked
                        }
                    });
                    localStorage.setItem('roughbook_oldNotes', JSON.stringify(oldNotes));
                    dispatch({ type: 'ALL_NOTES_CHECK_TOGGLED', payload: false })
                }
                dispatch({ type: 'LOADING_STOP'});
            })();
            break;
        case 'CHECK_AUDIO_ACCESS':
            chrome.runtime.sendMessage('phomkpbjipcgmfpffhkaphddmclefofh', {
                type: 'micAccess'
            });
            break;
        case 'SAVE_NEW_REMINDER':
            if (state.reminderLabel.trim().length > 0 && state.reminderDateTime !== undefined) {
                (async () => {
                    dispatch({ type: 'LOADING_START'});
                    const eventTimeStamp = new Date(state.reminderDateTime).getTime();
                    chrome.runtime.sendMessage('phomkpbjipcgmfpffhkaphddmclefofh', {
                        type: 'notification',
                        options: {
                            title: 'Roughbook wanted to notify you..',
                            message: state.reminderLabel,
                            iconUrl: '/logo48.png',
                            type: 'basic',
                            priority: 2,
                            requireInteraction: true,
                            eventTime: parseInt(eventTimeStamp)
                        }
                    });

                    const reminderToSave = "#RoughbookReminder for " + state.reminderDateTime.replace("T", " ") + " with note - \"" + state.reminderLabel + "\"";

                    const currentDate = Date.now();
                    const linkifiedNewNotes = linkify(reminderToSave);
                    const hashifiedNewNotes = hashify(linkifiedNewNotes);

                    const payload = {
                        createdOn: currentDate,
                        email: state.email.toLowerCase(),
                        notes: { statement: hashifiedNewNotes, striked: false },
                        action: "ADD_NEW_NOTES"
                    }
                    const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                        method: 'POST',
                        body: JSON.stringify(payload)
                    });

                    const responseJson = await response.json();
                    if (responseJson.statusCode === 200) {
                        dispatch({ type: 'SAVE_NEW_REMINDER_SUCCESS', payload: { currentDate, hashifiedNewNotes } });

                        let oldNotes = {
                            [currentDate]: { statement: hashifiedNewNotes, striked: false }
                        }
                        if (localStorage.getItem('roughbook_oldNotes') != null) {
                            oldNotes = {
                                ...JSON.parse(localStorage.getItem('roughbook_oldNotes')),
                                ...oldNotes
                            }
                        }
                        localStorage.setItem('roughbook_oldNotes', JSON.stringify(oldNotes));
                    }
                    dispatch({ type: 'LOADING_STOP'});
                })();
            }
            break;
        case 'ASK_NEW_FEATURE':
            (async () => {
                dispatch({ type: 'LOADING_START'});
                const payload = {
                    email: state.email.toLowerCase(),
                    newFeature: state.newFeature,
                    action: "ASK_NEW_FEATURE"
                }
                const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                    method: 'POST',
                    body: JSON.stringify(payload)
                });
                const responseJson = await response.json();
                if (responseJson.statusCode === 200) {
                    dispatch({ type: 'ASK_NEW_FEATURE_SUCCESS' });
                }
                dispatch({ type: 'LOADING_STOP'});
            })();
            break;
        case 'INSERT_LINK':
            const linkedText = state.newNotes + " ({" + state.linkText + "} linked to {" + state.linkURL +"})";
            
            if(state.newNotes === null || state.newNotes.trim().length === 0) {
                dispatch({ type: 'ADD_NEW_NOTES', payload: "({" + state.linkText + "} linked to {" + state.linkURL +"})" }); 
            } else {
                dispatch({ 
                    type: 'ADD_NEW_NOTES', 
                    payload:  linkedText}); 
            }
            dispatch({ type: 'INSERT_LINK_SUCCESS' });
            break;
        case 'UPDATE_IS_DARK':
            localStorage.setItem('roughbook_isDark', action.payload);
            dispatch({ type: 'UPDATE_IS_DARK_SUCCESS', payload: action.payload });
            const payloadToUpdateDark = {
                email: state.email.toLowerCase(),
                isDark: action.payload,
                action: "UPDATE_IS_DARK"
            }
            fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                method: 'POST',
                body: JSON.stringify(payloadToUpdateDark)
            });
            break;
        case 'SAVE_AUDIO':
            (async () => {
                dispatch({ type: 'LOADING_START'});
                if(state.audioBlob !== undefined) {
                    const duration = getAudioDuration(state.audioBlob.startTime, state.audioBlob.stopTime);
                    const hashifiedNewNotes = hashify("#audio of approx duration" + duration);
                    const reader = new FileReader();
                    reader.readAsDataURL(state.audioBlob.blob);
                    reader.onloadend = async () => {
                        const payload = {
                            createdOn: state.audioBlob.startTime,
                            email: state.email.toLowerCase(),
                            notes: { 
                                statement: hashifiedNewNotes, 
                                audioBlob: reader.result, 
                                striked: false 
                            },
                            action: "ADD_NEW_NOTES"
                        }
                        const response = await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                            method: 'POST',
                            body: JSON.stringify(payload)
                        });
                        const responseJson = await response.json();
                        if (responseJson.statusCode === 200) {
                            dispatch({ 
                                type: 'SAVE_AUDIO_SUCCESS', 
                                payload: { 
                                    currentDate: state.audioBlob.startTime, 
                                    audioS3: "roughbook/" + state.email.toLowerCase() + "_" + state.audioBlob.startTime, 
                                    hashifiedNewNotes: hashifiedNewNotes
                                } 
                            });
                            dispatch({type: 'UPDATE_AUDIO_URL', 
                                payload: {
                                    timestamp: state.audioBlob.startTime, 
                                    localURL: state.audioBlob.blobURL
                                }
                            });
                            let oldNotes = {
                                [state.audioBlob.startTime]: { 
                                    statement: hashifiedNewNotes, 
                                    audioS3: "roughbook/" + state.email.toLowerCase() + "_" + state.audioBlob.startTime, 
                                    striked: false 
                                }
                            }
                            if (localStorage.getItem('roughbook_oldNotes') != null) {
                                oldNotes = {
                                    ...JSON.parse(localStorage.getItem('roughbook_oldNotes')),
                                    ...oldNotes
                                }
                            }
                            localStorage.setItem('roughbook_oldNotes', JSON.stringify(oldNotes));
                        }
                    };
                }
                dispatch({ type: 'LOADING_STOP'});
            })();
            break;
        case 'ARRANGE_OLD_NOTES': 
            arrangeNotes(state, dispatch)
            break;
        case 'CLOSE_TOUR':
            localStorage.setItem('roughbook_tourDone', true)
            break;
        case 'START_TOUR':
            localStorage.removeItem('roughbook_tourDone')
            break;   
        case 'DOWNLOAD_AUDIO':
            dispatch({
                type: 'UPDATE_AUDIO_URL', 
                payload: {
                    timestamp: action.payload.timestamp, 
                    localURL: "downloading"
                }
            });
            const payload = {
                audioS3: action.payload.audioS3,
                action: "FETCH_AUDIO_BLOB"
            }
            
            fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
                method: 'POST',
                body: JSON.stringify(payload)
            })
            .then(response => response.json())
            .then((responseJson) => {
                fetch(responseJson)
                .then(ref => ref.blob())
                .then(blob => URL.createObjectURL(blob))
                .then(localURL => {
                    dispatch({
                        type: 'UPDATE_AUDIO_URL', 
                        payload: {
                            timestamp: action.payload.timestamp, 
                            localURL
                        }
                    });
                })
            });
            break;
        default: {
            dispatch(action);
            break;
        }
    };
};

const arrangeNotes = (state, dispatch) => {
    const arrangedNotes = {};
    const hashMap = {};
    const selectedNote = {};
    const allStrikedNotes = {};
    const deletedNotes = {};
    let deletedNotesCount = 0;
    const sortedTimestamps = [...Object.keys(state.oldNotes)].sort();
    let recentHashes = state.recentlyUsedHashtags;
    (sortedTimestamps).forEach((timestamp) => {
        selectedNote[timestamp] = false;

        const convertedDate = convertStampDate(timestamp);
        if (arrangedNotes[convertedDate] === undefined) {
            arrangedNotes[convertedDate] = [];
        }

        if (allStrikedNotes[convertedDate] === undefined) {
            allStrikedNotes[convertedDate] = true;
        }

        if (state.oldNotes[timestamp].striked === false) {
            allStrikedNotes[convertedDate] = false;
        } else if(state.oldNotes[timestamp].striked === true) {
            deletedNotesCount = deletedNotesCount+1;
            if(deletedNotes[convertedDate] === undefined) {
                deletedNotes[convertedDate] = []
            }

            deletedNotes[convertedDate].push({ timestamp, 
                notes: state.oldNotes[timestamp]
            }); 
        }

        arrangedNotes[convertedDate].push({ timestamp, 
            notes: state.oldNotes[timestamp]
        });

        let hashWords = getHashWord(state.oldNotes[timestamp].statement);
        if (hashWords != null) {
            hashWords.forEach((word) => {
                if (hashMap[word] === undefined) {
                    hashMap[word] = [];
                }

                hashMap[word].push(convertedDate);
                const index = recentHashes.indexOf(word);
                if (index > -1) {
                    recentHashes.splice(index, 1);
                }

                recentHashes.push(word)
            })
        }
    });
    dispatch({ 
        type: 'ARRANGE_OLD_NOTES_SUCCESS', 
        payload: { 
            arrangedNotes, 
            allStrikedNotes, 
            selectedNote,
            recentlyUsedHashtags: recentHashes.slice(-3).reverse(),
            hashMap,
            deletedNotes,
            deletedNotesCount
        } 
    });
}

const getHashWord = (statement) => {
    return statement.match(/(#[a-z\d-]+)/ig);
}

const convertStampDate = (timestamp) => {
    var months_arr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var date = new Date(parseInt(timestamp));
    var year = date.getFullYear();
    var month = months_arr[date.getMonth()];
    var day = date.getDate();
    var hours = "0" + date.getHours();
    var minutes = "0" + date.getMinutes();

    var fulldate = month + ' ' + day + ' ' + year + ' ' + hours.substr(-2) + ':' + minutes.substr(-2);
    return fulldate;
}

const getAudioDuration = (startTime, stopTime) => {
    let duration = '';
    let delta = Math.abs(stopTime - startTime);

    const days = Math.floor(delta / 86400 / 1000);
    delta -= (days * 86400 * 1000);
    if(days !== 0) {
        duration = " " + days + " days,"
    }
    const hours = Math.floor(delta / 3600 / 1000);
    delta -= (hours * 3600 * 1000);
    if(hours !== 0) {
        duration += " " + hours + " hours,"
    }

    const minutes = Math.floor(delta / 60 / 1000);
    delta -= (minutes * 60 * 1000);
    if(minutes !== 0) {
        duration += " " + minutes + " minutes,"
    }

    const seconds = Math.floor(delta / 1000);
    if(seconds !== 0) {
        duration += " " + seconds + ' seconds'
    }
    return duration;
}

const updateUserProfile = (email, key, value) => {
    (async () => {
        const payload = {
            email: email.toLowerCase(),
            key: key,
            value: value,
            action: "UPDATE_USER_PROFILE"
        }
        await fetch('https://i5870mwfv5.execute-api.ap-southeast-1.amazonaws.com/prod/notes', {
            method: 'POST',
            body: JSON.stringify(payload)
        });
    })();
}

function linkify(newNote) {
    const linked_text = /\(\{(.+)\}\slinked\sto\s\{(\b(https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])\}\)/igm; 
    const reformedNewNote1 = newNote.replace(linked_text, "<a style=\"color:rgb(26,175,220);\" target=\"_blank\" href='$2'>$1</a>");

    const linked_non_http_text = /\(\{(.+)\}\slinked\sto\s\{(|[^\/])([\S]+(\b|$))\}\)/igm; 
    const reformedNewNote2 = reformedNewNote1.replace(linked_non_http_text, "<a style=\"color:rgb(26,175,220);\" target=\"_blank\" href='http://$3'>$1</a>");

    const start_url = /(^(https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]\/?)/igm;
    const reformedNewNote3 = reformedNewNote2.replace(start_url, "<a style=\"color:rgb(26,146,220);\" target=\"_blank\" href='$1'>$1</a>");

    const mid_url = /(\s+((https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]\/?))/igm;
    const reformedNewNote4 = reformedNewNote3.replace(mid_url, " <a style=\"color:rgb(26,146,220);\" target=\"_blank\" href='$1'>$1</a>");

    const non_http_mid_url = /(^|[^\/])\s+(www\.[\S]+(\b|$))/gim;
    const reformedNewNote5 = reformedNewNote4.replace(non_http_mid_url, "$1 <a style=\"color:rgb(26,146,220);\" target=\"_blank\" href='http://$2'>$2</a>");
    
    const non_http_start_url = /^(www\.[\S]+(\b|$))/gim;
    return reformedNewNote5.replace(non_http_start_url, "<a style=\"color:rgb(26,146,220);\" target=\"_blank\" href='http://$1'>$1</a>");
}

const hashify = (newNotes) => {
    return newNotes.replace(/(^|\s)(#[a-z\d-]+)/ig, "$1<span style=\"color:rgb(26,146,220);font-weight:bold\">$2</span>");
}
