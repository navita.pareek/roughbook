const AppReducer = (state, action) => {
    let oldNotes = {};
    switch (action.type) {
        case 'LOADING_START':
            return {
                ...state,
                loading: true
            }
        case 'LOADING_STOP':
            return {
                ...state,
                loading: false
            }    
        case 'ADD_NEW_NOTES':
            localStorage.setItem('roughBook_newNotes', action.payload);
            return {
                ...state,
                newNotes: action.payload
            };
        case 'UPDATE_SIGN_IN_FORM_USERNAME':
            return {
                ...state,
                signInForm: {
                    ...state.signInForm,
                    username: action.payload
                }
            }
        case 'UPDATE_SIGN_IN_FORM_EMAIL':
            return {
                ...state,
                signInForm: {
                    ...state.signInForm,
                    email: action.payload
                }
            }
        case 'UPDATE_SIGN_IN_FORM_PASSCODE':
            return {
                ...state,
                signInForm: {
                    ...state.signInForm,
                    passcode: action.payload
                }
            }
        case 'SIGN_IN_SUCCESS':
            return {
                ...state,
                username: action.payload.username,
                email: state.signInForm.email,
                passcode: state.signInForm.passcode,
                oldNotes: action.payload.notes,
                enterToSaveNotes: action.payload.enterToSaveNotes,
                enterToSaveReminder: action.payload.enterToSaveReminder,
                signInForm: {},
                audioURLs: {}
            }
        case 'REFETCH_NOTES_SUCCESS':
            return {
                ...state,
                oldNotes: action.payload.notes,
                toSearchHash: [],
            }
        case 'SAVE_NEW_NOTES_SUCCESS':
            localStorage.removeItem('roughBook_newNotes');
            return {
                ...state,
                oldNotes: {
                    ...state.oldNotes,
                    [action.payload.currentDate]: {
                        statement: action.payload.hashifiedNewNotes,
                        striked: false
                    },
                },
                newNotes: "",
                reminderLabel: ""
            }
        case 'SAVE_AUDIO_SUCCESS':
            return {
                ...state,
                audioBlob: undefined,
                oldNotes: {
                    ...state.oldNotes,
                    [action.payload.currentDate]: {
                        statement: action.payload.hashifiedNewNotes,
                        audio: action.payload.audio,
                        striked: false
                    },
                }
            }
        case 'EMPTY_AUDIO_BLOB':
            return {
                ...state,
                audioBlob: undefined
            }
        case 'SAVE_NEW_REMINDER_SUCCESS':
            return {
                ...state,
                oldNotes: {
                    ...state.oldNotes,
                    [action.payload.currentDate]: {
                        statement: action.payload.hashifiedNewNotes,
                        striked: false
                    },
                },
                newNotes: "",
                reminderLabel: ""
            }
        case 'UPDATE_FROM_LOCAL_STORAGE':
            oldNotes = JSON.parse(localStorage.getItem('roughbook_oldNotes'))
            return {
                ...state,
                email: localStorage.getItem('roughBook_email'),
                passcode: localStorage.getItem('roughBook_password'),
                username: localStorage.getItem('roughbook_username'),
                enterToSaveNotes: (localStorage.getItem('roughBook_enterToSaveNotes') === 'true'),
                enterToSaveReminder: (localStorage.getItem('roughBook_enterToSaveReminder') === 'true'),
                newNotes: localStorage.getItem('roughBook_newNotes'),
                oldNotes: oldNotes ?? {},
                isDark: (localStorage.getItem('roughbook_isDark') === 'true'),
            }
        case 'ARRANGE_OLD_NOTES_SUCCESS':
            return {
                ...state,
                arrangedNotes: action.payload.arrangedNotes,
                allArrangedNotes: action.payload.arrangedNotes,
                allStrikedNotes: action.payload.allStrikedNotes,
                selectedNote: action.payload.selectedNote,
                recentlyUsedHashtags: action.payload.recentlyUsedHashtags,
                hashMap: action.payload.hashMap,
                deletedNotes: action.payload.deletedNotes,
                deletedNotesCount: action.payload.deletedNotesCount
            }
        case 'LOAD_MATCHED_NOTES':
            let newArrangedNotes = {};
            action.payload.forEach((searchHash) => {
                state.hashMap[searchHash].forEach((date) => {
                    newArrangedNotes[date] = state.allArrangedNotes[date];
                });
            });

            return {
                ...state,
                arrangedNotes: newArrangedNotes
            };
        case 'NOTE_CHECK_TOGGLED':
            return {
                ...state,
                selectedNote: {
                    ...state.selectedNote,
                    [action.payload.timestamp]: action.payload.checked
                }
            };
        case 'ALL_NOTES_CHECK_TOGGLED':
            const allSelectedNotes = {}

            if (action.payload === false) {
                Object.values(state.arrangedNotes).forEach((notes) => {
                    notes.forEach((note) => {
                        allSelectedNotes[note.timestamp] = false;
                    })
                });
            } else {
                Object.values(state.arrangedNotes).forEach((notes) => {
                    notes.forEach((note) => {
                        allSelectedNotes[note.timestamp] = true;
                    })
                });
            }

            return {
                ...state,
                allNotesChecked: action.payload,
                selectedNote: allSelectedNotes
            };
        case 'LOAD_ALL_NOTES':
            return {
                ...state,
                arrangedNotes: state.allArrangedNotes
            }
        case 'UPDATE_ERROR':
            return {
                ...state,
                error: action.payload
            }
        case 'SIGN_OUT':
            return {
                newNotes: "",
                oldNotes: {},
                email: "",
                passcode: "",
                signInForm: {},
                arrangedNotes: {},
                hashMap: {},
                allArrangedNotes: {},
                selectedNote: {},
                toSearchHash: [],
                recentlyUsedHashtags: [],
                reminderLabel: "",
                allNotesChecked: false,
                enterToSaveNotes: false,
                enterToSaveReminder: false,
                error: "Login/Register to proceed.",
                linkText: "",
                linkURL: "",
                isDark: true,
                audioURLs: {}
            }
        case 'DELETE_NOTES_SUCCESS':
            return {
                ...state,
                oldNotes: {
                    ...state.oldNotes,
                    [action.payload.createdOnDate]: {
                        ...state.oldNotes[action.payload.createdOnDate],
                        striked: action.payload.newStrike
                    }
                }
            }
        case 'UPDATE_SEARCH_HASH':
            return {
                ...state,
                toSearchHash: action.payload
            }
        case 'UPDATE_NEW_REMINDER_LABEL':
            return {
                ...state,
                reminderLabel: action.payload
            }
        case 'UPDATE_REMINDER_DATE_TIME':
            return {
                ...state,
                reminderDateTime: action.payload
            }
        case 'USE_ENTER_TO_SAVE_NOTE':
            return {
                ...state,
                enterToSaveNotes: action.payload
            }
        case 'USE_ENTER_TO_SAVE_REMINDER':
            return {
                ...state,
                enterToSaveReminder: action.payload
            }
        case 'UPDATE_NEW_FEATURE':
            return {
                ...state,
                newFeature: action.payload
            }
        case 'ASK_NEW_FEATURE_SUCCESS':
            return {
                ...state,
                newFeature: undefined
            }
        case 'UPDATE_NEW_HASHTAGS':
            return {
                ...state,
                newHashtags: action.payload
            }
        case 'SAVE_EDITED_NOTE_SUCCESS':
            let history;
            if(state.oldNotes[action.payload.timestamp].history !== undefined) {
                history = state.oldNotes[action.payload.timestamp].history;
                history.push(
                {
                    timestamp: Date.now(), 
                    statement: action.payload.statement
                });
            } else {
                history = [{
                    timestamp: action.payload.timestamp, 
                    statement: state.oldNotes[action.payload.timestamp].statement
                },
                {
                    timestamp: Date.now(), 
                    statement: action.payload.statement
                }];
            }
            return {
                ...state,
                oldNotes: { 
                    ...state.oldNotes, 
                    [action.payload.timestamp]: {
                        ...state.oldNotes[action.payload.timestamp],
                        history,
                        statement: action.payload.statement,
                    }
                }
            }  
        case 'CHECK_HISTORY': 
            return {
                ...state,
                history: action.payload
            }      
        case 'ADD_NEW_HASHTAGS_SUCCESS':
            oldNotes = { ...state.oldNotes };
            action.payload.forEach((value) => {
                oldNotes[value.timestamp].statement = value.statement;
            });

            return {
                ...state,
                oldNotes,
                newHashtags: ""
            }
        case 'UPDATE_LINK_TEXT':
            return {
                ...state,
                linkText: action.payload
            }   
        case 'UPDATE_LINK_URL':
            return {
                ...state,
                linkURL: action.payload
            }  
        case 'INSERT_LINK_SUCCESS': 
            return {
                ...state,
                linkText: "",
                linkURL: ""
            }   
        case 'UPDATE_IS_DARK_SUCCESS':
            return {
                ...state,
                isDark: action.payload
            } 
        case 'UPDATE_AUDIO':
            return {
                ...state,
                audioBlob: action.payload
            }  
        case 'UPDATE_AUDIO_URL':
            return {
                ...state,
                audioURLs: 
                {
                    ...state.audioURLs,
                    [action.payload.timestamp]: action.payload.localURL
                }
            } 
        case 'ADD_TO_EDIT_NOTES':
            return {
                ...state,
                editNotes: {
                    ...state.editNotes,
                    statement: action.payload
                }
            }     
        case 'SAVE_NOTE_TO_EDIT':
            return {
                ...state,
                editNotes: {
                    ...state.editNotes,
                    statement: action.payload.statement,
                    timestamp: action.payload.timestamp ? action.payload.timestamp : state.editNotes.timestamp
                }
            }           
        default: return state;
    };
};

export default AppReducer;

